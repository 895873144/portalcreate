package tlv.enums;

public enum ErrCode {
	Success(100, "联网认证成功"), Refused(101, "连接拒绝"), Already(102, "已建立链接"), Busy(103, "设备繁忙，请稍后重试"), Fail(104,
			"认证失败,账号或密码错误"), LogoutSuccess(701, "下线成功"), LogoutFail(702, "下线失败");
	private final int errCode;
	private final String mark;

	private ErrCode(int errCode, String mark) {
		this.errCode = errCode;
		this.mark = mark;
	}

	public int getErrCode() {
		return errCode;
	}

	public String getMark() {
		return mark;
	}
}
