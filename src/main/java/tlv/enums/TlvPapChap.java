package tlv.enums;

public enum TlvPapChap {
	Chap(0), Pap(1);
	private final int PapChaptype;

	private TlvPapChap(int type) {
		this.PapChaptype = type;
	}

	public int getPapChap() {
		return PapChaptype;
	}
}
