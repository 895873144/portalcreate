package tlv.enums;

public enum TlvType {
	ReqChallenge(1), ReqAuth(3), ReqLogout(5), AffAckauth(7), ReqInfo(9);
	private final int type;

	private TlvType(int type) {
		this.type = type;
	}

	public int getType() {
		return type;
	}
}
