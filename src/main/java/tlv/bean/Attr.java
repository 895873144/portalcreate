package tlv.bean;

public class Attr {
	private int attrType;
	private int attrLen;
	private byte[] attrValue;

	public int getAttrType() {
		return attrType;
	}

	public void setAttrType(int attrType) {
		this.attrType = attrType;
	}

	public int getAttrLen() {
		return attrLen;
	}

	public void setAttrLen(int attrLen) {
		this.attrLen = attrLen;
	}

	public Attr() {
		super();
	}

	public byte[] getAttrValue() {
		return attrValue;
	}

	public void setAttrValue(byte[] attrValue) {
		this.attrValue = attrValue;
	}
	


	public Attr(int attrType, byte[] attrValue) {
		super();
		this.attrType = attrType;
		this.attrLen = 2+attrValue.length;
		this.attrValue = attrValue;
	}
}
