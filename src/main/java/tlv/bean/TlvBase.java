package tlv.bean;

import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.List;

public class TlvBase {
	protected int ver;
	protected int type;
	protected int papChap = 0;
	protected int rsvd = 0;
	protected int serialNo;
	protected int reqId;
	protected String userIp;
	protected int userPort = 0;
	protected int errCode;
	protected int attrNum;
	protected List<Attr> attr = new ArrayList<Attr>();

	public int getVer() {
		return ver;
	}

	public void setVer(int ver) {
		this.ver = ver;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public int getSerialNo() {
		return serialNo;
	}

	public void setSerialNo(int serialNo) {
		this.serialNo = serialNo;
	}

	public int getReqId() {
		return reqId;
	}

	public void setReqId(int reqId) {
		this.reqId = reqId;
	}

	public String getUserIp() {
		return userIp;
	}

	public void setUserIp(String userIp) {
		this.userIp = userIp;
	}

	public int getErrCode() {
		return errCode;
	}

	public void setErrCode(int errCode) {
		this.errCode = errCode;
	}

	public int getAttrNum() {
		return attrNum;
	}

	public void setAttrNum(int attrNum) {
		this.attrNum = attrNum;
	}

	public List<Attr> getAttr() {
		return attr;
	}

	public void setAttr(List<Attr> attr) {
		this.attr = attr;
	}
	
	public int getPapChap() {
		return papChap;
	}

	public void setPapChap(int papChap) {
		this.papChap = papChap;
	}
	
	public TlvBase(){
	}
	
	public void build(int ver, int type, int serialNo, int reqId, String userIp, int errCode, int attrNum,
			List<Attr> attr){
		this.ver = ver;
		this.type = type;
		this.serialNo = serialNo;
		this.reqId = reqId;
		this.userIp = userIp;
		this.errCode = errCode;
		this.attrNum = attrNum;
		this.attr = attr;
	}
	
	public void build(byte[] a,int attr_start){
		this.ver = a[0];
		this.type = a[1];
		this.serialNo = (int)((a[5]<<8)|(a[4]));
		this.reqId = (int)((a[6]<<8)|(a[7]));
		this.userIp = (int)a[8]+"."+(int)a[9]+"."+(int)a[10]+"."+(int)a[11];
		this.errCode = a[14];
		this.attrNum = a[15];
		this.attr = parseAttr(a,attr_start);
	}
	
	protected List<Attr> parseAttr(byte[] a,int start){
		List<Attr> attr = new ArrayList<Attr>();
		int count = a[15];
		if(count>0){
			int t = start;
			for (int i = 0; i < count; i++) {
				Attr c = new Attr();
				c.setAttrType(a[t]);
				c.setAttrLen(a[t+1]);
				byte[] d = new byte[c.getAttrLen()-2];
				System.arraycopy(a, t+2, d, 0, c.getAttrLen()-2);
				t += c.getAttrLen();
				c.setAttrValue(d);
				attr.add(c);
			}
		}
		return attr;
	}
	
	/**工具方法-Begin**/
	/**
	 * 将ip地址转为byte[]
	 * @param ipAdd
	 * @return
	 */
	protected byte[] ipv4Address2BinaryArray(String ipAdd) {
		byte[] binIP = new byte[4];
		String[] strs = ipAdd.split("\\.");
		for (int i = 0; i < strs.length; i++) {
			binIP[i] = (byte) Integer.parseInt(strs[i]);
		}
		return binIP;
	}
	
	/**
	 * 将mac地址转为byte[]
	 * @param ipAdd
	 * @return
	 */
	protected byte[] mac2BinaryArray(String mac) {
		byte[] binMac = new byte[6];
		String[] strs = mac.split(":");
		for (int i = 0; i < strs.length; i++) {
			binMac[i] = (byte)Integer.valueOf(strs[i],16).intValue();
		}
		return binMac;
	}
	
	/**
	 * 对字符串md5加密
	 *
	 * @param str
	 * @return
	 */
	protected byte[] getMD5(byte[] a) {
		try {
			// 生成一个MD5加密计算摘要
			MessageDigest md = MessageDigest.getInstance("MD5");
			// 计算md5函数
			md.update(a);
			// digest()最后确定返回md5 hash值，返回值为8为字符串。因为md5 hash值是16位的hex值，实际上就是8位的字符
			// BigInteger函数则将8位的字符串转换成16位hex值，用字符串来表示；得到字符串形式的hash值
			return md.digest();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	/**工具方法-End**/

}
