package tlv.test;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import tlv.Utils;
import tlv.socket.AbloomyPapAuth;
import tlv.socket.AbloomyPapLogout;
import tlv.socket.call.AbloomyPapAuthCall;
import tlv.socket.call.AbloomyPapLogoutCall;
import tlv.socket.call.SkspruceChapAuthCall;
import tlv.socket.call.SkspruceChapLogoutCall;

/**
 * 韵盛发设备测试
 * 
 * @author yet
 *
 */
public class AbloomyTest {

	/**
	 * 上线请求测试
	 */
	public static void PapAuthTest() {
		new AbloomyPapAuth("192.168.1.198", 2000, "172.16.0.6", "ac:c1:ee:3c:84:43", "qcp", "123456",Utils.getRandom(65025, 0)).start();
	}

	/**
	 * 发起下线请求测试
	 */
	public static void PapLogoutTest() {
		new AbloomyPapLogout("192.168.1.198", 2000, "172.16.0.6", "ac:c1:ee:3c:84:43", Utils.getRandom(65025, 0)).start();
	}
	


	/**
	 * 上线请求测试
	 */
	public static void PapAuthCallTest() {
		try {
			ExecutorService pool = Executors.newFixedThreadPool(10);
			Callable<String> c1 = new AbloomyPapAuthCall("192.168.1.198", 2000, "172.16.0.6", "ac:c1:ee:3c:84:43", "qcp", "123456",Utils.getRandom(65025, 0));
			Future<String> f1 = pool.submit(c1);
			System.out.println(f1.get());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 发起下线请求测试
	 */
	public static void PapLogoutCallTest() {
		try {
			ExecutorService pool = Executors.newFixedThreadPool(10);
			Callable<String> c1 = new AbloomyPapLogoutCall("192.168.1.198", 2000, "172.16.0.6", "ac:c1:ee:3c:84:43", Utils.getRandom(65025, 0));
			Future<String> f1 = pool.submit(c1);
			System.out.println(f1.get());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
