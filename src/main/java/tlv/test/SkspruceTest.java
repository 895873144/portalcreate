package tlv.test;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import tlv.Utils;
import tlv.socket.SkspruceChapAuth;
import tlv.socket.SkspruceChapLogout;
import tlv.socket.call.SkspruceChapAuthCall;
import tlv.socket.call.SkspruceChapLogoutCall;

/**
 * 西加云杉700E设备测试
 * 
 * @author yet
 *
 */
public class SkspruceTest {

	/**
	 * 上线请求测试
	 */
	public static void ChapAuthTest() {
		new SkspruceChapAuth("192.168.1.199", 2000, "192.168.1.200", "zyt", "123456", Utils.getRandom(65025, 0))
				.start();
	}

	/**
	 * 发起下线请求测试
	 */
	public static void ChapLogoutTest() {
		new SkspruceChapLogout("192.168.1.199", 2000, "192.168.1.200", "zyt", "123456", Utils.getRandom(65025, 0))
				.start();
	}

	/**
	 * 上线请求测试
	 */
	public static void ChapAuthCallTest() {
		try {
			ExecutorService pool = Executors.newFixedThreadPool(10);
			Callable<String> c1 = new SkspruceChapAuthCall("192.168.1.199", 2000, "192.168.1.200", "zyt", "123456",
					Utils.getRandom(65025, 0));
			Future<String> f1 = pool.submit(c1);
			System.out.println(f1.get());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 发起下线请求测试
	 */
	public static void ChapLogoutCallTest() {
		try {
			ExecutorService pool = Executors.newFixedThreadPool(10);
			Callable<String> c1 = new SkspruceChapLogoutCall("192.168.1.199", 2000, "192.168.1.200", "zyt", "123456",
					Utils.getRandom(65025, 0));
			Future<String> f1 = pool.submit(c1);
			System.out.println(f1.get());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
