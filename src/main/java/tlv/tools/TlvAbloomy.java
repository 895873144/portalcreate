package tlv.tools;

import java.util.List;

import tlv.bean.Attr;
import tlv.bean.TlvBean;
import tlv.enums.TlvType;

/**
 * 封装向西加云杉700E设备的TLV包
 * 
 * @author yet
 *
 */
public class TlvAbloomy {
	public static byte[] buildAbloomy(TlvType tlvType, int serialNo, int reqId, String userIp, List<Attr> attr) {
		TlvBean b = new TlvBean(tlvType.getType(), serialNo, reqId, userIp, 0, attr.size(), attr);
		b.setPapChap(1);
		return b.toByteArray();
	}

	public static TlvBean parse(byte[] a) {
		return new TlvBean(a);
	}
}
