package tlv.tools;

import java.util.List;

import tlv.Constants;
import tlv.bean.Attr;
import tlv.bean.TlvBean;
import tlv.bean.TlvHwBean;

public class TlvTools {
	public static byte[] buildHwReqChallenge(int serialNo,String userIp,List<Attr> attr){
		TlvHwBean b = new TlvHwBean(1, serialNo, 0, userIp, 0, attr.size(), attr, Constants.SECRET);
		return b.toByteArray();
	}
	public static byte[] buildHwReqAuth(int serialNo,int reqId,String userIp,List<Attr> attr){
		TlvHwBean b = new TlvHwBean(3, serialNo, reqId, userIp, 0, attr.size(), attr, Constants.SECRET);
		return b.toByteArray();
	}
	public static byte[] buildHwReqLogout(int serialNo,int reqId,String userIp,List<Attr> attr){
		TlvHwBean b = new TlvHwBean(5, serialNo, reqId, userIp, 0, attr.size(), attr, Constants.SECRET);
		return b.toByteArray();
	}
	public static byte[] buildHwAffAckauth(int serialNo,int reqId,String userIp,List<Attr> attr){
		TlvHwBean b = new TlvHwBean(7, serialNo, reqId, userIp, 0, attr.size(), attr, Constants.SECRET);
		return b.toByteArray();
	}
	public static byte[] buildHwReqInfo(int serialNo,int reqId,String userIp,List<Attr> attr){
		TlvHwBean b = new TlvHwBean(9, serialNo, reqId, userIp, 0, attr.size(), attr, Constants.SECRET);
		return b.toByteArray();
	}
	public static TlvHwBean parseHw(byte[] a){
		return new TlvHwBean(a);
	}

	public static byte[] buildReqChallenge(int serialNo,String userIp,List<Attr> attr){
		TlvBean b = new TlvBean(1, serialNo, 0, userIp, 0, attr.size(), attr);
		return b.toByteArray();
	}
	public static byte[] buildReqAuth(int serialNo,int reqId,String userIp,List<Attr> attr){
		TlvBean b = new TlvBean(3, serialNo, reqId, userIp, 0, attr.size(), attr);
		return b.toByteArray();
	}
	public static byte[] buildReqLogout(int serialNo,int reqId,String userIp,List<Attr> attr){
		TlvBean b = new TlvBean(5, serialNo, reqId, userIp, 0, attr.size(), attr);
		return b.toByteArray();
	}
	public static byte[] buildAffAckauth(int serialNo,int reqId,String userIp,List<Attr> attr){
		TlvBean b = new TlvBean(7, serialNo, reqId, userIp, 0, attr.size(), attr);
		return b.toByteArray();
	}
	public static byte[] buildReqInfo(int serialNo,int reqId,String userIp,List<Attr> attr){
		TlvBean b = new TlvBean(9, serialNo, reqId, userIp, 0, attr.size(), attr);
		return b.toByteArray();
	}
	public static TlvBean parse(byte[] a){
		return new TlvBean(a);
	}
}
