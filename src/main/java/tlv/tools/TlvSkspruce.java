package tlv.tools;

import java.util.List;

import tlv.Constants;
import tlv.bean.Attr;
import tlv.bean.TlvHwBean;
import tlv.enums.TlvPapChap;
import tlv.enums.TlvType;

/**
 * 封装向西加云杉700E设备的TLV包
 * 
 * @author yet
 *
 */
public class TlvSkspruce {
	public static byte[] buildSkspruce(TlvType tlvType, int serialNo, int reqId, String userIp,
			List<Attr> attr) {
		TlvHwBean b = new TlvHwBean(tlvType.getType(), serialNo, reqId, userIp, 0, attr.size(), attr, Constants.SECRET);
		return b.toByteArray();
	}

	public static TlvHwBean parseHw(byte[] a) {
		return new TlvHwBean(a);
	}
}
