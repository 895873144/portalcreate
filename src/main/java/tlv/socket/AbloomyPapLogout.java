package tlv.socket;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import tlv.bean.Attr;
import tlv.bean.TlvBean;
import tlv.enums.TlvType;
import tlv.tools.TlvAbloomy;


public class AbloomyPapLogout extends Thread {
	private DatagramSocket s;
	private InetAddress hostAddress;
	private byte[] buf = new byte[1000];
	private DatagramPacket dp = new DatagramPacket(buf, buf.length);
	private String ip;
	private int port;
	private String userIp;
	private int serialNo;
	private String mac;
	private TlvBean b = new TlvBean();

	public void run() {
		try {
			s.send(Dgram.toDatagram(ReqLogout(), hostAddress, port));
			s.receive(dp);
			b = parse(dp.getData());
			if(b.getErrCode()==0){
				System.out.println("下线成功");
			}else{
				System.out.println("下线失败");
			}
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		}
	}
	
	/**
	 * 生成强制下线请求
	 * @return
	 */
	private byte[] ReqLogout(){
		List<Attr> attr = new ArrayList<Attr>();
		Attr a = new Attr(238, mac2BinaryArray(mac));
		attr.add(a);
		return TlvAbloomy.buildAbloomy(TlvType.ReqChallenge,serialNo, 00, userIp, attr);
	}

	/**
	 * 将byte[]转为TlvHwBean
	 * @param a
	 * @return
	 */
	private TlvBean parse(byte[] a){
		return TlvAbloomy.parse(a);
	}
	

	
	/**
	 * 将mac地址转为byte[]
	 * @param ipAdd
	 * @return
	 */
	private byte[] mac2BinaryArray(String mac) {
		byte[] binMac = new byte[6];
		String[] strs = mac.split(":");
		for (int i = 0; i < strs.length; i++) {
			binMac[i] = (byte)Integer.valueOf(strs[i],16).intValue();
		}
		return binMac;
	}

	/**
	 * 
	 * @param ip AC的ip地址
	 * @param port AC监听的端口
	 * @param userIp 用户内网ip
	 */
	public AbloomyPapLogout(String ip, int port,String userIp,String mac,int serialNo) {
		this.ip = ip;
		this.port = port;
		this.userIp = userIp;
		this.serialNo = serialNo;
		this.mac = mac;
		try {
			s = new DatagramSocket();
			hostAddress = InetAddress.getByName(this.ip);

		} catch (UnknownHostException e) {
			System.err.println("Cannot find host");
			System.exit(1);
		} catch (SocketException e) {
			System.err.println("Can't open socket");
			e.printStackTrace();
			System.exit(1);
		}
		System.out.println("请求开始");
	}
}
