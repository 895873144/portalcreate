package tlv.socket;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import tlv.bean.Attr;
import tlv.bean.TlvHwBean;
import tlv.enums.TlvType;
import tlv.tools.TlvSkspruce;

/**
 * 西加云杉700E设备请求下线
 * @author yet
 *
 */
public class SkspruceChapLogout extends Thread {
	private DatagramSocket s;
	private InetAddress hostAddress;
	private byte[] buf = new byte[1000];
	private DatagramPacket dp = new DatagramPacket(buf, buf.length);
	private int port;
	private String userIp;
	private int serialNo;
	private String username;
	private String password;

	public void run() {
		try {
			s.send(Dgram.toDatagram(HwReqLogout(), hostAddress, port));
			s.receive(dp);
			TlvHwBean b = parse(dp.getData());
			if(b.getErrCode()==0){
				System.out.println("下线成功");
			}else{
				System.out.println("下线失败");
			}
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		}
	}
	
	/**
	 * 生成强制下线请求
	 * @return
	 */
	private byte[] HwReqLogout(){
		List<Attr> attr = new ArrayList<Attr>();
		Attr a = new Attr(1, username.getBytes());
		Attr b = new Attr(2, password.getBytes());
		attr.add(a);
		attr.add(b);
		return TlvSkspruce.buildSkspruce(TlvType.ReqLogout,serialNo, 00, userIp, attr);
	}
	
	/**
	 * 将byte[]转为TlvHwBean
	 * @param a
	 * @return
	 */
	private TlvHwBean parse(byte[] a){
		return TlvSkspruce.parseHw(a);
	}
	
	/**
	 * 
	 * @param ip AC的ip地址
	 * @param port AC监听的端口
	 * @param userIp 用户内网ip
	 */
	public SkspruceChapLogout(String ip, int port,String userIp,String username,String password,int serialNo) {
		this.port = port;
		this.userIp = userIp;
		this.serialNo = serialNo;
		this.username = username;
		this.password = password;
		try {
			s = new DatagramSocket();
			hostAddress = InetAddress.getByName(ip);

		} catch (UnknownHostException e) {
			System.err.println("Cannot find host");
			System.exit(1);
		} catch (SocketException e) {
			System.err.println("Can't open socket");
			e.printStackTrace();
			System.exit(1);
		}
		System.out.println("请求开始");
	}
}
