package tlv;

import java.util.Random;

public class Utils {
	/**
	 * 用于生成随机的serialNo
	 * @param max
	 * @param min
	 * @return
	 */
	public static int getRandom(int max, int min) {
		Random r = new Random();
		int s = r.nextInt(max) % (max - min + 1) + min;
		return s;
	}
}
